package entity.order.enums;

public enum OrderStatusEntity {

    COMPLETED,
    PROCESSING,
    ON_HOLD

}
