package entity.customer.enums;

public enum CustomerGenderEntity {

    MALE,
    FEMALE,
    OTHER

}
